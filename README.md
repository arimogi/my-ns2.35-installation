# My NS2.35 Installation

My NS2.35 Installation. Installed on Ubuntu 18.04. Adapted from **Something4NS2**

- [NS \(Simulator\) - Wikipedia](https://en.wikipedia.org/wiki/Ns_(simulator))
- [The Network Simulator - NS2](https://www.isi.edu/nsnam/ns/)


## Download

You can download NS2 version 2.35 from NSNAM. Here is the link: <br /> 
> [ns-allinone-2.35.tar.gz](http://sourceforge.net/projects/nsnam/files/allinone/ns-allinone-2.35/ns-allinone-2.35.tar.gz/download)
<br />

To verify after download, you can use this checksum. <br />
> 2216f4e8e274f5c2437741fc6e9c9728369fabe1838c708ef974d262b941cd5d  -- *SHA256*
  

## Extract

Extract the `ns-allinone-2.35.tar.gz` file to path or folder you chose.

```shell
cd /to/your/path/
tar -xvf ns-allinone-2.35.tar.gz
```

After extraction, a folder named `ns-allinone-2.35` will created.


## Build

*Tested on Ubuntu 18.04.1 and Ubuntu 18.04.2*<br />
Before you can build NS2 from script, you might need several preparation. Below are the needs for preparation. You can skip the step if you are already have those.

### Dependency

1. Install ```xorg-dev``` for ns2.

```shell
sudo apt install -y xorg-dev
```

2. Install `maker` and `compiler`. Needed to run `install` script from NS2.

```shell
sudo apt install -y build-essential
```

3. Install `g++-5` if you are using Ubuntu 17.10+. NS2 only can be compiled using `g++` version 5 below.

```shell
sudo apt install -y g++-5
```

4. Install library `libperl4-corelibs-perl`. Needed to run some scripts from the simulation example scripts.

```shell
sudo apt install -y libperl4-corelibs-perl
```

5. Install  `libotcl1 libtclcl1`. Needed by nam. If you will not use nam from NS-AllInOne, you can skip this step.

```shell
sudo apt install -y libotcl1 libtclcl1
```

6. Install Network Animator `nam`.
```shell
sudo apt install -y nam
```


### Debug

The error that appeared when you `build` NS2 directly from the source might be like these.

```
In file included from linkstate/ls.cc:67:0:
linkstate/ls.h: In instantiation of ‘void LsMap<Key, T>::eraseAll() [with Key = int; T = LsIdSeq]’:
linkstate/ls.cc:396:28:   required from here
linkstate/ls.h:137:25: error: ‘erase’ was not declared in this scope, and no declarations were found by argument-dependent lookup at the point of instantiation [-fpermissive]
  void eraseAll() { erase(baseMap::begin(), baseMap::end()); }
                    ~~~~~^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
linkstate/ls.h:137:25: note: declarations in dependent base ‘std::map<int, LsIdSeq, std::less<int>, std::allocator<std::pair<const int, LsIdSeq> > >’ are not found by unqualified lookup
linkstate/ls.h:137:25: note: use ‘this->erase’ instead
```

The compiler cannot find 'erase' function or declaration, because the different scope. We can add 'this' to define the scope, before the 'erase' function. You can use `vi`, `nano`, or your favorite text editor. Here is using `vi`.

```shell
vi /the/path/to/ns-allinone-2.35/ns-2.35/linkstate/ls.h
```

Find this line.

```CPP
  void eraseAll() { erase(baseMap::begin(), baseMap::end()); }
```

Add `this->` right before 'erase'.

```CPP
  void eraseAll() { this->erase(baseMap::begin(), baseMap::end()); }
```

### Install

```shell
cd /the/path/to/ns-allinone-2.35/
export CC=gcc-5 CXX=g++-5 && ./install
```

### Set Environment Variables

Set ```PATH``` on user `.bashrc` file. Put this code after the last line on `.bashrc` file.

```bash
export NS_HOME=/the/path/to/ns-allinone-2.35
PATH=$NS_HOME/bin:$NS_HOME/tcl8.5.10/unix:$NS_HOME/tk8.5.10/unix:$PATH
export LD_LIBRARY_PATH=$NS_HOME/otcl-1.14:$NS_HOME/lib
export TCL_LIBRARY=$NS_HOME/tcl8.5.10/library
```

### Validate

You can skip this process, but it recommended to do. This process will take 1 up to 3 hours to finish. Please, consider your need.

```shell
cd /the/path/to/ns-allinone-2.35/ns-2.35/
./validate
```

### Nam problem
If you found error like these:
`nam: Can't find a usable tk.tcl in the following directories`. You can try to install dependencies needed by nam (Network Animation).

```shell
sudo apt install -y libotcl1 libtclcl1
```

## Discussion
You can send me an email to arimogi_at_gmail.com if you have something to discuss.


**Cheerio**

Ari Mogi
